#!/usr/bin/env python

import os
import sys
import codecs
from numpy import array
import nltk
import random 
import csv
import sklearn
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
import scipy
import pickle

USAGE = """PREFIXUS. A system for binary classification of Russian prefixes.

Usage:\t./%s <mode> [<data>]
<mode>\tUse "train" to train classification models and test their performance; use "classify" to classification of new examples. In the train mode, the program will save the models in the directory 'models'. In the classification mode, the program will use all classifiers from the 'models' directory and save them next to the input data file with an extension corresponding to the used model, e.g. 'test.csv.NaiveBayes'.
<data>\tA CSV file with data in UTF-8 encoding in the following format: 'prefix;word;word-without-prefix'. The program assumes that words/prefixes are in Russian, default = data.csv. 
""" % os.path.basename(sys.argv[0])

# Constants
MIN_ARG_NUM = 2 # program name + required parameters
TOKENS_FILE = "tokens.csv" # dictionary of tokens
LEMMAS_FILE = "lemmas.csv" # dictionary of lemmas
DATA_FILE = "data.csv" # training/classification data
CV_NUM = 100 # number of cross validations
CV_RATIO = 0.1 # percentage of test examples, e.g. 0.1 will split as 90/10
TRAIN_MODE = "train" 
CLASSIFY_MODE = "classify"
MODELS_DIR = "./models"
MAXS_FILE = MODELS_DIR + "/maxs-23" # file needed for feature normalization at classification time 

#FEATURES_LIST = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23] # all 23 features
FEATURES_LIST = [1, 2, 3, 4, 10, 11, 12, 16, 17, 18, 20, 21, 22, 23] # 14 selected features

# Global variables
_tokens = {} # dictionary of tokens
_lemmas = {} # dictionary of lemmas

def main():
	# Set the parameters 
	if len(sys.argv) == 2 and sys.argv[1] in ["help", "-help", "-h", "h"]:
		print USAGE
		sys.exit()
	if len(sys.argv) < MIN_ARG_NUM:
	        print USAGE
		sys.exit()
	
	if sys.argv[1] in [TRAIN_MODE, CLASSIFY_MODE]:
		mode = sys.argv[1]
	else:
		print USAGE
		sys.exit()
	
	data_fname = DATA_FILE 
	if len(sys.argv) >= MIN_ARG_NUM + 1:
		data_fname = sys.argv[2]

	# Load the data
	data = load_data(data_fname)
	global _tokens
	_tokens = load_tokens(TOKENS_FILE)
	global _lemmas
	_lemmas = load_lemmas(LEMMAS_FILE)
	fmatrix = get_fmatrix(data)

	if mode == TRAIN_MODE:
		# Train the model and test it
		print_features_stat(data, True)
		train_and_test(fmatrix, True, True)
		print_average_scores(fmatrix)
	elif mode == CLASSIFY_MODE:
		classify(data, data_fname)

def classify(data, data_fname):		
	"""Performs classification of the new examples 'data' loaded from the 'data_fname'
	and saves the result next to this file.
	"""
	# Classify new data
	print "Classification of %d examples from %s..." % (len(data), data_fname)

	# Load the classifier
	for classifier_fname in [f for f in os.listdir(MODELS_DIR) if f.endswith(".pickle")]:
				
		classifier_file = open(MODELS_DIR + "/" + classifier_fname)
		classifier = pickle.load(classifier_file)
		classifier_file.close()
		output_fname = data_fname + "." + classifier_fname[:-7]
		output_file = open(output_fname, 'w')
		print "%s: data classified with the '%s' model" % (output_fname, classifier_fname[:-7])

		has_prob = (hasattr(classifier, 'prob_classify') and
				"LinearSVC" not in classifier_fname and
				"DecisionTree" not in classifier_fname)

		for row in data:
			f = get_features(row["prep"], row["word-wo-prep"], row["word"], row["pos"], True)
			l = classifier.classify(f)
			p = classifier.prob_classify(f).prob(l) if has_prob else 1.0
			output_file.write("%f;%s;%s;%s;%s;%s;%s\n" % (p, l, row["class"], row["prep"], row["pos"], row["word-wo-prep"], row["word"]))

		output_file.close()
		
	print "Done!"

def get_fmatrix(data):
	""" Builds a feature matrix for the data. The features are normalized.
	"""
	fmatrix = ([(get_features(row["prep"], row["word-wo-prep"], row["word"], row["pos"], False), row["class"]) for row in data])
	
	# Normalize the variables
	maxs = {}
	for i, row in enumerate(fmatrix):
		for var in row[0]:
			#if i <= 30: print "{", var, ":", row[0][var], "},"
			if not var in maxs: maxs[var] = row[0][var]
			elif maxs[var] < row[0][var]: maxs[var] = row[0][var]
	
	for i, row in enumerate(fmatrix):
		for var in row[0]:
			row[0][var] = row[0][var]/maxs[var]
			#if i <= 30: print "{", var, ":", row[0][var], "},"
	
	# Save maxs to disk
	f = open(MODELS_DIR + "/maxs", "wb")
	pickle.dump(maxs, f)
	f.close()

	return fmatrix

def print_average_scores(fmatrix):
	"""
	Prints average scores of classifier metrics -- accuracy, precision, recall, fmeasure
	"""

	print "\n\n===================================\nAverage scores\n"
	accuracy = {}
	precision = {}
	recall = {}
	fmeasure = {}
	for i in xrange(CV_NUM):
		scores = train_and_test(fmatrix, False, False)
		for model in scores:
			if not model in accuracy: accuracy[model] = []
			if not model in precision: precision[model] = []
			if not model in recall: recall[model] = []
			if not model in fmeasure: fmeasure[model] = []

			accuracy[model].append(scores[model][0])
			precision[model].append(scores[model][1])
			recall[model].append(scores[model][2])
			fmeasure[model].append(scores[model][3])
	
	#print accuracy #, precision, recall, fmeasure
	for model in accuracy:
		print "\n%s\n----------------------------------" % model
		accuracy_num = array(accuracy[model])
		precision_num = array(precision[model])
		recall_num = array(recall[model])
		fmeasure_num = array(fmeasure[model])
		print "Accuracy = %5.3f +- %5.3f" % (accuracy_num.mean(), accuracy_num.std())
		print "Precision = %5.3f +- %5.3f" % (precision_num.mean(), precision_num.std())
		print "Recall = %5.3f +- %5.3f" % (recall_num.mean(), recall_num.std())
		print "Fmeasure = %5.3f +- %5.3f" % (fmeasure_num.mean(), fmeasure_num.std())
	
def load_tokens(fname):
	"""Reads a list of tokens from a utf-8 CSV file 'fname' in the follwing format: 'freq;token'
	"""
	reader = csv.DictReader(open(fname, "r"), delimiter=';', quotechar="'", quoting=csv.QUOTE_MINIMAL)
	tokens = {}
	for row in reader:
		token = row["token"].strip()
		if not tokens.has_key(token):
			tokens[token] = float(row["freq"])
		else:
			print "Duplicate token", token
	print len(tokens), "tokens loaded"
	
	#for i, t in enumerate(tokens):
	#	if i < 30:
	#		print "==%s==%s==" % (t, tokens[t])

	return tokens

def load_lemmas(fname):
	"""Reads a list of lemmas from a utf-8 CSV file 'fname' in the follwing format: 'freq;lemma;pos'
	"""
	reader = csv.DictReader(open(fname, "r"), delimiter=';', quotechar="'", quoting=csv.QUOTE_MINIMAL)
	lemmas = {}
	for row in reader:
		lemma = row["lemma"].strip()
		freq = float(row["freq"])
		pos = row["pos"].strip()
		if not lemmas.has_key(lemma):
			lemmas[lemma] = [(freq, pos)]
		else:
			lemmas[lemma].append((freq, pos))
	print len(lemmas), "lemmas loaded"

	#for i, l in enumerate(lemmas):
	#	if i < 30:
	#		print "==%s==%s==" % (l lemmas[l])

	return lemmas

def load_data(fname):
	""" Reads input data (training data or new data to classify) in memory.
	"""

	reader = csv.DictReader(open(fname, "r"), delimiter=';', quotechar="'", quoting=csv.QUOTE_MINIMAL)
	reader_list = []
	for row in reader:
		row_dict = {}
		row_dict["class"] = row["class"]
		row_dict["prep"] = row["prep"].strip()
		row_dict["word"] = row["word"].strip()
		row_dict["word-wo-prep"] = row["word-wo-prep"].strip()
		row_dict["pos"] = row["pos"].strip().upper()
		reader_list.append(row_dict)

	print len(reader_list), "training examples loaded"
	return reader_list

def get_features(pref, word_wo_pref, word, pos, normalize):
	""" Extracts features representing a word and its (candidate) prefix. 
	"""
	
	features = {}

	if 1 in FEATURES_LIST:
		features["word_in_lemmas"] = float(_lemmas.has_key(word))

	if 2 in FEATURES_LIST:
		features["is_prep"] = 0
		if pref in _lemmas:
			for i in _lemmas[pref]:
				if i[1] == "prep": features["is_prep"] = 1

	if 3 in FEATURES_LIST:
		features["wordwo_in_lemmas"] = float(_lemmas.has_key(word_wo_pref))

	if 4 in FEATURES_LIST:
		features["pref_in_lemmas"] = float(_lemmas.has_key(pref))

	if 5 in FEATURES_LIST:
  		features["word_in_tokens"] = float(_tokens.has_key(word))

	if 6 in FEATURES_LIST:
		features["pref_in_tokens"] = float(_tokens.has_key(pref)) 

	if 7 in FEATURES_LIST:
		features["wordwo_in_tokens"] = float(_tokens.has_key(word_wo_pref))

	if 8 in FEATURES_LIST:
		word_lfreq = _lemmas[word][0][0] if word in _lemmas else 0
		wordwo_lfreq = _lemmas[word_wo_pref][0][0] if word_wo_pref in _lemmas else 0
		features["rel_lfreq"] = (wordwo_lfreq / word_lfreq) if word_lfreq > 0 else 0

	if 9 in FEATURES_LIST:
		word_tfreq = _tokens[word] if word in _tokens else 0
		wordwo_tfreq = _tokens[word_wo_pref] if word_wo_pref in _tokens else 0
		features["rel_tfreq"] = (wordwo_tfreq / word_tfreq) if word_tfreq > 0 else 0

	if 10 in FEATURES_LIST:
		features["word_tfreq"] = _tokens[word] if word in _tokens else 0

	if 11 in FEATURES_LIST:
		features["wordwo_tfreq"] = _tokens[word_wo_pref] if word_wo_pref in _tokens else 0

	if 12 in FEATURES_LIST:
		features["pref_tfreq"] = _tokens[pref] if pref in _tokens else 0

	if 13 in FEATURES_LIST:
		features["word_lfreq"] = _lemmas[word][0][0] if word in _lemmas else 0

	if 14 in FEATURES_LIST:
		features["wordwo_lfreq"] = _lemmas[word_wo_pref][0][0] if word_wo_pref in _lemmas else 0

	if 15 in FEATURES_LIST:
		features["pref_lfreq"] =  _lemmas[pref][0][0] if pref in _lemmas else 0

	if 16 in FEATURES_LIST:
		features["has_dash"] = float("-" in word)

	if 17 in FEATURES_LIST:
		features["pref_len"] = float(len(pref))

	if 18 in FEATURES_LIST:
		features["wordwo_len"] = float(len(word_wo_pref))

	if 19 in FEATURES_LIST:
		features["rel_len"] = float(len(pref)) / float(len(word_wo_pref))

	if 20 in FEATURES_LIST:
		features["is_adj"] = float(pos == "A" or pos == "ADJ")

	if 21 in FEATURES_LIST:
		pref_u = unicode(pref, "utf-8")
		features["starts_with_p"] = float(pref_u[0] == u"\u043f")

	if 22 in FEATURES_LIST:
		features["starts_with_n"] = float(pref_u[0] == u"\u043d")

	if 23 in FEATURES_LIST:
		word_pos = _lemmas[word][0][1] if word in _lemmas else "unkn" 
		wordwo_pos =  _lemmas[word_wo_pref][0][1] if word_wo_pref in _lemmas else "unkn" 
		features["same_pos"] = word_pos == wordwo_pos
	
	# Normalize the features
	if normalize and os.path.exists(MAXS_FILE):
		# Load maximums for each feature
		maxs_file = open(MAXS_FILE)
		maxs = pickle.load(maxs_file)
		maxs_file.close()

		# Normalize each feature
		for n in features:
			if n in maxs and not maxs[n] == 0: 
				features[n] = features[n]/maxs[n]
	
	#features["random"] = random.random()
	return features

def corr(var, labels):
	print scipy.stats.pearsonr(var, labels)

def train_and_test(fmatrix, print_scores, save_models):
	"""Train models on the data provided in the 'fmatrix' and return a tuple of scores (accuracy, precision, recall, fmeasure).
	If 'print_scores', then the scores are printed to the standard output.
	If 'save_models', then the trained classifiers are saved on the disk. 
	"""
	# Split the data according to the CV_RATIO, e.g. 0.1 will split as 90/10
	random.shuffle(fmatrix)
	split_i = max(int(round(len(fmatrix) * CV_RATIO)), 1)
	train_set, test_set = fmatrix[split_i:], fmatrix[:split_i]
	
	# Train classifiers 
	classifiers = [
			("NaiveBayes", nltk.NaiveBayesClassifier.train(train_set)),
			("LinearSVC", nltk.classify.scikitlearn.SklearnClassifier(LinearSVC()).train(train_set)),
			("LogisticRegression", nltk.classify.scikitlearn.SklearnClassifier(LogisticRegression(C=1e5)).train(train_set)),
			("SVC-linear", nltk.classify.scikitlearn.SklearnClassifier(SVC(kernel='linear', probability=True)).train(train_set)),
			("SVC-poly", nltk.classify.scikitlearn.SklearnClassifier(SVC(kernel='poly', degree=3, probability=True)).train(train_set)),
			("SVC-rbf", nltk.classify.scikitlearn.SklearnClassifier(SVC(kernel='rbf', probability=True)).train(train_set)),
			("DecisionTree", nltk.classify.decisiontree.DecisionTreeClassifier.train(train_set)),
			]

	# Evaluate and save the classifiers
	scores = {} 
	for c in classifiers:
		# Performance scores 
		scores[c[0]] = get_scores(c[0], c[1], test_set, print_scores)	

		# Save to disk 
		if save_models:
			f = open(MODELS_DIR + "/" + c[0] + ".pickle", "wb")
			pickle.dump(c[1], f)
			f.close()

	return scores
	
def get_scores(classifier_name, classifier, test_set, print_scores):
	""" Calculates performance metrics of a 'classifier' with the name 'name_classifier'
	on the 'test_set'. Scores returned as a tuple (accuracy, precision, recall, fmeasuere).
	If 'print_scores', then the scores are printed to the standard output.
	"""

	# Confusion matrix
	if print_scores:
		print "\n\n===================================\n", classifier_name, "\n"

		ref  = [g for (n,g) in test_set] 
		test = [classifier.classify(n) for (n,g) in test_set]
		cm = nltk.ConfusionMatrix(ref, test)
		print cm.pp(sort_by_count=True, show_percents=False, truncate=9)
		print cm.pp(sort_by_count=True, show_percents=True, truncate=9) 

	# Accuracy, precision, recall, and fmeasure metrics 
	ref = set()
	test = set()
	for i, (n,g) in enumerate(test_set):
		if g == "1":
			ref.add(i)
		
		c = classifier.classify(n)
		if c == "1":
			test.add(i)
		#print i, g, c
	#print ref
	#print test
	precision = nltk.metrics.scores.precision(ref, test)
	if precision is None: precision = 0
	recall = nltk.metrics.scores.recall(ref, test)
	if recall is None: recall = 0
	fmeasure = nltk.metrics.scores.f_measure(ref, test)
	if fmeasure is None: fmeasure = 0
	accuracy = nltk.classify.accuracy(classifier, test_set)
	if accuracy is None: accuracy = 0
	scores = (accuracy, precision, recall, fmeasure)
	if print_scores:
		print "Accuracy = %5.3f, Precision = %5.3f, Recall = %5.3f, Fmeasure = %5.3f" % scores
	return scores

def print_features_stat(data, print_data):
	"""Prints non normalized features and their correlations with the target class
	"""
	SHOW_MAX_ROWS = 10
	fmatrix_ex = ([(get_features(row["prep"], row["word-wo-prep"], row["word"], row["pos"], False), row["prep"], row["word"], row["word-wo-prep"], row["class"]) for row in data])
	variables = {}
	labels = []

	if print_data: print "\n==================================\nExamples of the features\n"

	for i, row in enumerate(fmatrix_ex):
		# Print the row
		if print_data and i <= SHOW_MAX_ROWS:
			print row[1], row[2], row[3], row[4], "="

		# Construct matrix of variables for correlation
		for j, var in enumerate(row[0]):
			if print_data and i <= SHOW_MAX_ROWS:
				print "{", var, ":", row[0][var], "},"

			if not var in variables:
				variables[var] = []
			variables[var].append(float(row[0][var]))
		labels.append(float(row[4]))
		if print_data and i <= SHOW_MAX_ROWS: print ""

	print "\n==================================\nCorrelations of the features with the class\n"
	for var in variables:
		print var, corr(variables[var], labels)

if __name__ == '__main__':
	main()
