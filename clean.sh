#!/bin/bash

rm -f *.DecisionTree*
rm -f *.LinearSVC*
rm -f *.LogisticRegression*
rm -f *.NaiveBayes*
rm -f *.SVC-linear*
rm -f *.SVC-poly*
rm -f *.SVC-rbf*

rm -f ./models/DecisionTree.pickle
rm -f ./models/LinearSVC.pickle
rm -f ./models/LogisticRegression.pickle
rm -f ./models/NaiveBayes.pickle
rm -f ./models/SVC-linear.pickle
rm -f ./models/SVC-poly.pickle
rm -f ./models/SVC-rbf.pickle
