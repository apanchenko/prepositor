prefixus
========

Данная программа отличает настоящие и продуктивные приставки слов русского языка от просто случайных совпадений слов по началу. 

Правильные приставки:

- агро S агрошкола школа
- до A докоммунистический коммунистический

Ошибочные приставки:

- эргон A эргономический омический
- прос S простачка тачка 

Файлы
-----

- *prefixus.py* -- программа (скрипт) 
- *clean.sh* -- файл для удаления результатов классификации и новых моделей (не удаляет базовые модели)
- *data.csv* -- исходная обучающая выборка  
- *test.csv* -- несколько дополнительных примеров 
- *lemmas.csv* -- частотный словарь лемм
- *tokens.csv* -- частотный словарь словоформ
- *report.odt*, *report.pdf* -- исходный текст отчета и отчет
- *models/* -- директория с 14 моделями (7 используют 14 признаков, остальные 7 все 23 признака, все модели обучены на всех 440 примерах)
	1. DecisionTree-14.pickle
	2. LinearSVC-14.pickle
	3. LogisticRegression-14.pickle
	4. NaiveBayes-14.pickle
	5. SVC-linear-14.pickle
	6. SVC-poly-14.pickle
	7. SVC-rbf-14.pickle
	8. DecisionTree-23.pickle
	9. LinearSVC-23.pickle
	10. LogisticRegression-23.pickle
	11 NaiveBayes-23.pickle
	12. SVC-linear-23.pickle
	13. SVC-poly-23.pickle
	14. SVC-rbf-23.pickle

Все текствые файлы используемые в программе в формате utf-8.

Требования
----------

- Python 2.7.3 или выше

Установка в Ubuntu Linux
------------------------

0. sudo apt-get install python-pip python-scipy python-numpy
1. sudo pip install nltk
2. sudo pip install scikit-learn

Использование
-------------

Usage:	./prefixus.py <mode> [<data>]

<mode>	Use "train" to train classification models and test their performance; use "classify" to classification of new examples. In the train mode, the program will save the models in the directory 'models'. In the classification mode, the program will use all classifiers from the 'models' directory and save them next to the input data file with an extension corresponding to the used model, e.g. 'test.csv.NaiveBayes'.

<data>	A CSV file with data in UTF-8 encoding in the following format: 'prefix;word;word-without-prefix'. The program assumes that words/prefixes are in Russian, default = data.csv. 

Формат файла с результатом следующий:

```
0.764100;1;0;у;S;слуга;услуга
0.556613;0;1;кзыл;S;орда;кзыл-орда
0.989333;1;1;с;S;рубка;срубка
0.880693;1;0;га;S;бровка;габровка
0.978511;1;1;при;S;кладка;прикладка
0.795716;1;1;плодо;S;ножка;плодоножка
0.984829;1;1;севморнефте;S;геофизика;севморнефтегеофизика
0.528804;1;0;ма;S;тематика;математика
0.960521;1;1;с;S;пайка;спайка
0.639897;0;0;жи;S;галка;жигалка

```

- Первая колонка -- вероятность предсказания
- Вторая колонка -- класс
- Третья колонка -- класс, указанный в исходном файле
- Четвертая колонка и далее -- как в исходном файле

Изначально результаты отсортированы как в исходном файле. Более удобно анализировать результаты отсортированные по вероятности классификации, например так

```
sort -r data.csv.LogisticRegression 

```

Результат сортировки:

```

1.000000;1;1;не;A;сертифицированный;несертифицированный
0.999999;1;1;не;S;формальность;неформальность
0.999999;1;1;не;S;сознательности;несознательности
0.999996;1;1;не;ADV;одобрительно;неодобрительно
0.999994;1;1;не;A;свойственному;несвойственному
0.999994;1;1;не;A;парнокопытное;непарнокопытное
0.999985;1;1;не;A;кавказский;некавказский
0.999978;1;1;не;A;экономичною;неэкономичною
0.999978;1;1;не;A;расторопных;нерасторопных
0.999978;1;1;не;A;гигиеничный;негигиеничный

```

